# README #

### simple java 8 concurrency example. ###

A simple java 8 maven project showing the usage of ExecutorService with a fixed thread pool size.
Uses callable (not runnable) by adding return parameter, see submit()...