package com.j8.concurrency1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * return params in submit --> Callable used
 */
public class Service {

    public static void main(String[] args) throws InterruptedException,
            ExecutionException {
        ExecutorService service = null;
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        int tasks = 10;
        System.out.println("processors: " + availableProcessors);
        int threads = availableProcessors * 2;
        System.out.println("thread pool size: " + threads);
        try {
            service = Executors.newFixedThreadPool(threads);
            List<Future> futures = new ArrayList<Future>();
            for (int i=0;i<tasks;i++) {
                Future<String> result = service.submit(() -> {
                    String name = new Service().getName();
                    return name;
                });
                futures.add(result);
                System.out.println("task " + (i+1) + " started!");
            }
            for (Future future : futures) {
                System.out.println(future.get(10, TimeUnit.SECONDS) + " done!");
            }
        } catch (TimeoutException e) {
            System.out.println("Not reached in time");
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }
    }

    public String getName() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        return Thread.currentThread().toString();
    }

}
